# Flight-Booking



## Getting started

This project is developed using the VIP structure and includes various modules and libraries to achieve the desired functionality. It utilizes Alamofire, Moya, SwiftyJson, a network manager class for network reachability, custom camera permission classes, SkyFloatingTextField, custom alert view, custom "No Record Found" class, Swagger for API implementation, a Constants folder for all constant definitions, a common validation file, UI component extensions for advanced UI changes, a custom user default class for saving user data in local storage, a custom color constant file for customizing theme colors, and a key messages file for managing alert messages.

# Modules


## Book Flight
The Book Flight module allows users to book flights by providing necessary information. It handles the communication with the server and provides a user-friendly interface to book flights.

## View: 
The view component is responsible for presenting the user interface for flight booking. It includes forms for entering flight details such as departure and arrival locations, dates, and passenger information.

## Interactor: 
The interactor component contains the business logic for flight booking. It interacts with the data layer to fetch available flights, perform booking validations, and make API requests for flight booking.

## Presenter: 
The presenter component acts as the intermediary between the view and the interactor. It receives user input from the view, processes it, and communicates with the interactor to perform the necessary operations. It also handles the presentation of flight options and booking results back to the view.



# Libraries
The following libraries are used in this project:

Alamofire: Alamofire is a widely-used HTTP networking library for iOS and macOS. It simplifies the process of making network requests and handling responses.

Moya: Moya is a network abstraction layer that helps in building network requests with ease. It works on top of Alamofire and provides a more structured approach to handling network requests.

SwiftyJson: SwiftyJson is a library that makes it easy to deal with JSON data in Swift. It simplifies parsing and manipulating JSON objects and arrays.

#Classes

## Network Manager
The Network Manager class is responsible for managing network reachability. It provides methods to check if the device has an active internet connection and handles network status changes.

## Custom Camera Permission Classes

Custom Camera Permission Classes handle camera permission-related functionality. They provide methods to request camera access and handle camera permission status.

## SkyFloatingTextField

SkyFloatingTextField is a custom text field component that enhances the user interface by providing floating labels and other customization options.

## Custom Alert View

Custom Alert View provides a customizable alert view that can be used to display messages, notifications, and prompts to the user.

## Custom "No Record Found" Class
The Custom "No Record Found" class is used when there are no records available to display. It provides a user-friendly message or UI element indicating that no records are currently present.

##  API Implementation
This project utilizes Swagger for API implementation. Swagger is an open-source toolset that helps in designing, building, documenting, and consuming RESTful web services.

## Constants
The Constants folder contains files where all constant values are defined. These constants can include API endpoints, keys, strings, and other values used throughout the project.

## Common Validation
The common validation file provides reusable functions for validating user input and performing common validation tasks. It helps in ensuring that user input meets the required criteria and prevents invalid data from being processed.

## UI Component Extensions
The UI component extensions provide additional functionality and customization options for UI elements. They allow for advanced UI changes and enhancements in the project.

## Custom User Default Class
The custom user default class provides methods for saving and retrieving user data in the local storage using UserDefaults. It simplifies the process of managing user preferences and settings.

## Custom Color Constant
The custom color constant file contains predefined color constants that can be used throughout the project for consistent theming and styling.

## Key Messages
The key messages file stores alert messages and other text strings used in the project. It centralizes all the messages in one place for easy management and localization.

# Installation Steps
To run the project locally, follow these steps:

1 .Clone the repository: git clone <https://gitlab.com/git_mobile_lr/ios-sample-iii.git>
2. Install the necessary dependencies using a package manager such as CocoaPods or Swift Package Manager.
3. Open the project in Xcode.
4. Build and run the project on a simulator or a physical device.
