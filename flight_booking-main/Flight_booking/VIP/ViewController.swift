//
//  ViewController.swift
//  flight_booking
//
//  Created by Parth iOS  on 20/07/23.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    //----------------
    // MARK:- Actions-
    //----------------
    
    @IBAction func bookFlightAction(_ sender: UIButton) {
        let vC = BookFlightViewController.instantiateFromStoryboard(.BookFlight)
        self.navigationController?.pushViewController(vC, animated: true)
    }
 
}
