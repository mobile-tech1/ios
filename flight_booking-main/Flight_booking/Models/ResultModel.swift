//
//  ResultModel.swift
//  AircraftApp
//
//  Created by SOTSYS148 on 12/03/20.
//  Copyright © 2020 SOTSYS203. All rights reserved.
//

import UIKit

//class UserDM {
//    static var sharedUser: LoginResponseData!
//}

class BaseModel:NSObject{
    var responseCode: Int?
    var responseMessage: String = ""

    init(responseCode: Int?, responseMessage:String) {
        self.responseCode = responseCode
        self.responseMessage = responseMessage
    }
}
