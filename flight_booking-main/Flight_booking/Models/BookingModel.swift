//
//  BookingModel.swift
//  AircraftApp
//
//  Created by SOTSYS148 on 01/04/20.
//  Copyright © 2020 SOTSYS203. All rights reserved.
//

import UIKit

class BookingModel: NSObject {
    
    static var bookingSharedDM: BookingModel?
    var title: String? = ""
    var iTripType:Int? = 0
    var iDepartureTime:Int? = 1582628400
    var iDepartureDate:Int? = 1582628400
    var iReturnDate:Int? = 0
    var iReturnTime:Int? = 0
    var iTripBookingType:String? = "0"
    var isSourceSelect:Bool? = false
    var isDestinationSelect:Bool? = false
    var bookingParentId:Int? = 0
    var returnAircraftTypeId:Int? = 0
    var iBookingId:Int? = 0
    var date:String? = nil
    var time:String? = nil
    var selectedAircaft: AircraftTypesListResponseData?
    var departureAirport:AirportListResponseData? =  AirportListResponseData.init(iAirportId: 1, vId: "KAIK", vAirportName: "Aiken Regional Airport", vZipcode: "29805", vCity: "Aiken")
    var destinationAirport:AirportListResponseData? =  AirportListResponseData.init(iAirportId: 3, vId: "KAND", vAirportName: "Anderson Regional Airport", vZipcode: "29626", vCity: "Anderson")
    
     init(title: String?,iTripType:Int?,iDepartureTime:Int?,iDepartureDate:Int?,iReturnDate:Int?,iReturnTime:Int?,iTripBookingType:String?,isSourceSelect:Bool?,isDestinationSelect:Bool?,bookingParentId:Int?,returnAircraftTypeId:Int?,iBookingId:Int?,date:String?,time:String?,selectedAircaft: AircraftTypesListResponseData?,departureAirport:AirportListResponseData?,destinationAirport:AirportListResponseData?) {
        self.title = title
        self.iTripType = iTripType
        self.iDepartureTime = iDepartureTime
        self.iDepartureDate = iDepartureDate
        self.iReturnDate = iReturnDate
        self.iReturnTime = iReturnTime
        self.iTripBookingType = iTripBookingType
        self.isSourceSelect = isSourceSelect
        self.isDestinationSelect = isDestinationSelect
        self.bookingParentId = bookingParentId
        self.returnAircraftTypeId = returnAircraftTypeId
        self.iBookingId = iBookingId
        self.date = date
        self.time = time
        self.selectedAircaft = selectedAircaft
        self.departureAirport = departureAirport
        self.destinationAirport = destinationAirport
    }
}
