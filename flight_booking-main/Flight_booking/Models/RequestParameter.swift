//
//  RequestParameter.swift
//  CodeStructure
//
//  Created by Hitesh on 11/29/16.
//  Copyright © 2016 myCompany. All rights reserved.
//

import UIKit

class RequestParameter: NSObject {
    static var instance: RequestParameter!
    
//    var objUser : UserData! = nil
    let eDeviceType = "1"
    
    // SHARED INSTANCE
    class func sharedInstance() -> RequestParameter {
        self.instance = (self.instance ?? RequestParameter())
        return self.instance
    }
    
    func signUpStep1Param(vUserName : String, vPhone: String, vEmail: String, password: String, password_confirmation: String,vISDCode: String) -> Dictionary<String, Any> {
        var requestDictionary : Dictionary<String, Any> = Dictionary()
        requestDictionary["vUserName"] = vUserName
        requestDictionary["vPhone"] = vPhone
        requestDictionary["vEmail"] = vEmail
        requestDictionary["password"] = password
        requestDictionary["password_confirmation"] = password_confirmation
        requestDictionary["vISDCode"] = vISDCode
        return requestDictionary
    }
    
    func signUpStep2Param(iUserId: Int,vIdProof: String) -> Dictionary<String,Any> {
        var requestDictionary : Dictionary<String, Any> = Dictionary()
        requestDictionary["iUserId"] = iUserId
        requestDictionary["vIdProof"] = vIdProof
        return requestDictionary
    }
    
    func forgotPassword(vEmail : String) -> Dictionary<String,String>{
        var requestDictionary : Dictionary<String,String> = Dictionary()
        requestDictionary["vEmail"] = vEmail
        return requestDictionary
    }
    
    func changePasswordParam(cpassword : String, password: String, password_confirmation:String) -> Dictionary<String,String> {
        var requestDictionary : Dictionary<String,String> = Dictionary()
        requestDictionary["cpassword"] = cpassword
        requestDictionary["password"] = password;
        requestDictionary["password_confirmation"] = password_confirmation
        print(requestDictionary)
        return requestDictionary
    }
    
    func loginParam(vEmail : String, password: String) -> Dictionary<String,String> {
        
        var requestDictionary : Dictionary<String,String> = Dictionary()
        requestDictionary["vEmail"] = vEmail;
        requestDictionary["password"] = password
        //print(requestDictionary)
        return requestDictionary
    }

    func logoutParam(Authorization: String) -> Dictionary<String,String> {
        var requestDictionary : Dictionary<String,String> = Dictionary()
        requestDictionary["AccessToken"] = Authorization
        return requestDictionary
    }
    
    func resendOtp(iUserId: Int,vPhoneNo: String) -> Dictionary<String,Any>{
        var requestDictionary : Dictionary<String,Any> = Dictionary()
        requestDictionary["iUserId"] = iUserId
        requestDictionary["vPhoneNo"] = vPhoneNo
        return requestDictionary
    }
    
    func verifyOtp(iUserId: Int, iOtp: Int) -> Dictionary<String,Int>{
        var requestDictionary : Dictionary<String,Int> = Dictionary()
        requestDictionary["iUserId"] = iUserId
        requestDictionary["iOtp"] = iOtp
        return requestDictionary
    }
    
    func updateProfileParam(vUserName:String, vPhone:String, vEmail:String, vProfileImage:String, vIdProof:String, AccessToken:String) -> Dictionary<String,String> {
        var requestDictionary : Dictionary<String,String> = Dictionary()
        requestDictionary["vUserName"] = vUserName
        requestDictionary["vPhone"] = vPhone
        requestDictionary["vEmail"] = vEmail
        requestDictionary["vProfileImage"] = vProfileImage
        requestDictionary["vIdProof"] = vIdProof
        requestDictionary["AccessToken"] = AccessToken
        return requestDictionary
    }
    
    func selectAirportsParam(vZipcode: String, vCity: String) -> Dictionary<String,String>{
        var requestDictionary : Dictionary<String,String> = Dictionary()
        requestDictionary["vZipcode"] = vZipcode
        requestDictionary["vCity"] = vCity
        return requestDictionary
    }
    
    func searchAircraftParam(iTripType:String,iDepartureId:Int,iDestinationId: Int,iDepartureDate: Int,iDepartureTime:Int,iReturnDate:Int? = nil,iReturnTime:Int? = nil,iAircraftTypeId:Int,iTripBookingType:String,vDeviceUniqueId:String) -> Dictionary<String,Any> {
        var requestDictionary : Dictionary<String,Any> = Dictionary()
        requestDictionary["iTripType"] = iTripType
        requestDictionary["iDepartureId"] = iDepartureId
        requestDictionary["iDestinationId"] = iDestinationId
        requestDictionary["iDepartureDate"] = iDepartureDate
        requestDictionary["iDepartureTime"] = iDepartureTime
        requestDictionary["iReturnDate"] = iReturnDate
        requestDictionary["iReturnTime"] = iReturnTime
        requestDictionary["iAircraftTypeId"] = iAircraftTypeId
        requestDictionary["iTripBookingType"] = iTripBookingType
        requestDictionary["vDeviceUniqueId"] = vDeviceUniqueId
        return requestDictionary
    }
    
    func AirfraftDetailParam(iTripType:String,iTripBookingType:String,iBookingParentId:String? = nil,iDepartAircraftId:Int,iReturnAircraftId:Int? = nil,iDepartureId:Int,iDestinationId:Int,iDepartureDate:Int,iDepartureTime:Int,iReturnDate:Int? = nil,iReturnTime:Int? = nil) -> Dictionary<String,Any> {
        var requestDictionary : Dictionary<String,Any> = Dictionary()
        requestDictionary["iTripType"] = iTripType
        requestDictionary["iTripBookingType"] = iTripBookingType
        requestDictionary["iBookingParentId"] = iBookingParentId
        requestDictionary["iDepartAircraftId"] = iDepartAircraftId
        requestDictionary["iReturnAircraftId"] = iReturnAircraftId
        requestDictionary["iDepartureId"] = iDepartureId
        requestDictionary["iDestinationId"] = iDestinationId
        requestDictionary["iDepartureDate"] = iDepartureDate
        requestDictionary["iDepartureTime"] = iDepartureTime
        requestDictionary["iReturnDate"] = iReturnDate
        requestDictionary["iReturnTime"] = iReturnTime
        return requestDictionary
    }
    
    func boardingPassDetailParam(iTripId: String, AccessToken: String) -> Dictionary<String,String  >{
        var requestDictionary : Dictionary<String,String> = Dictionary()
        requestDictionary["iTripId"] = iTripId
        requestDictionary["AccessToken"] = AccessToken
        return requestDictionary
    }
    
    func bookingContinueParam(iTripType:String,iTripBookingType:String,iBookingParentId:String? = nil,iDepartAircraftId:Int,iReturnAircraftId:Int? = nil,iDepartureId:Int,iDestinationId:Int,iDepartureDate:Int,iDepartureTime:Int,iReturnDate:Int? = nil,iReturnTime:Int? = nil,dcTotalPrice:Double,iNumberOfSeats:Int) -> Dictionary<String,Any> {
        var requestDictionary : Dictionary<String,Any> = Dictionary()
        requestDictionary["iTripType"] = iTripType
        requestDictionary["iTripBookingType"] = iTripBookingType
        requestDictionary["iBookingParentId"] = iBookingParentId
        requestDictionary["iDepartAircraftId"] = iDepartAircraftId
        requestDictionary["iReturnAircraftId"] = iReturnAircraftId
        requestDictionary["iDepartureId"] = iDepartureId
        requestDictionary["iDestinationId"] = iDestinationId
        requestDictionary["iDepartureDate"] = iDepartureDate
        requestDictionary["iDepartureTime"] = iDepartureTime
        requestDictionary["iReturnDate"] = iReturnDate
        requestDictionary["iReturnTime"] = iReturnTime
        requestDictionary["dcTotalPrice"] = dcTotalPrice
        requestDictionary["iNumberOfSeats"] = iNumberOfSeats
        return requestDictionary
    }
    
    func addPassengerParam(iBookingId: Int, vPassengerName: String,vPassengerId: String,dPassengerDob: String) -> Dictionary<String,Any> {
        var requestDictionary : Dictionary<String,Any> = Dictionary()
        requestDictionary["iBookingId"] = iBookingId
        requestDictionary["vPassengerName"] = vPassengerName
        requestDictionary["vPassengerId"] = vPassengerId
        requestDictionary["dPassengerDob"] = dPassengerDob
        return requestDictionary
    }
    
    func cancelFlightParam(iTripId:String,iPassengerId:String,AccessToken:String) -> Dictionary<String,String> {
        var requestDictionary : Dictionary<String,String> = Dictionary()
        requestDictionary["iTripId"] = iTripId
        requestDictionary["iPassengerId"] = iPassengerId
        requestDictionary["AccessToken"] = AccessToken
        return requestDictionary
    }
    
    func dealsBookingContinueParam(iDealId: Int, tIsBookEntireFlight: String,iNumberOfSeats: String? = nil,dcTotalPrice: String? = nil) -> Dictionary<String,Any> {
        var requestDictionary : Dictionary<String,Any> = Dictionary()
        requestDictionary["iDealId"] = iDealId
        requestDictionary["tIsBookEntireFlight"] = tIsBookEntireFlight
        requestDictionary["iNumberOfSeats"] = iNumberOfSeats
        requestDictionary["dcTotalPrice"] = dcTotalPrice
        return requestDictionary
    }
}

