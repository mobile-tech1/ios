//
//  ColorConstants.swift
//  LR_BaseCode
//
//  Created by Admin on 28/3/2023.
//
import Foundation
import UIKit

let blackShadowColor = UIColor.color(FlightBooking.hexColor.blackShadow)?.withAlphaComponent(0.1).cgColor
let blueShadowColor = UIColor.color(FlightBooking.hexColor.blueShadow)?.withAlphaComponent(0.16).cgColor

public extension UIColor {
    class func color(_ hexString: String) -> UIColor? {
        if (hexString.count > 7 || hexString.count < 7) {
            return nil
        } else {
            let hexInt = Int(String(hexString[hexString.index(hexString.startIndex, offsetBy: 1)...]), radix: 16)
            if let hex = hexInt {
                let components = (
                    R: CGFloat((hex >> 16) & 0xff) / 255,
                    G: CGFloat((hex >> 08) & 0xff) / 255,
                    B: CGFloat((hex >> 00) & 0xff) / 255
                )
                return UIColor(red: components.R, green: components.G, blue: components.B, alpha: 1)
            } else {
                return nil
            }
        }
    }
    
}

struct FlightBooking {
    struct hexColor {
        static let blueShadow = "#2712BD"
        static let blackShadow =  "#000000"
    }
    enum name: String {
        case blackShadow, blueShadow
    }
}
