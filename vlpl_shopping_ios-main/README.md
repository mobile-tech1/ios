# VLPL_Shooping_iOS



## Getting started

This project is an iOS application that implements an e-commerce shopping cart feature. It follows the Model-View-Controller (MVC) architecture pattern and includes various custom components and classes for enhanced functionality and customization.

# Features
 - MVC structure for clean and organized codebase.
 - Integration of the e-commerce display cart data module.
 - Custom URLSession library for seamless API integration.
 - NetworkManager class for network reachability checks.
 - Custom camera permission classes for managing camera access.
 - Utilization of SkyFloatingTextField library for enhanced text input fields.
 - Custom alert view for displaying alerts and messages.
 - Custom NoRecordView class for handling empty cart scenarios.
 - Constant folder containing all the defined constants.
 - Common validation file for reusable validation functions.
 - UI component extensions for advanced UI changes and customization.
 - CustomUserDefaults class for saving user data in local storage.
 - Custom colorconstant.swift file for customizable theme colors.
 - KeyMessages.swift file for managing alert messages.
 
# Installation

 - Clone the repository: git clone https://github.com/username/repository.git
 - Open the project in Xcode.
 - Build and run the application on the desired iOS device or simulator.
 
## Requirements
 - iOS 12.0+
 - Xcode 11.0+
 - Swift 5.0+
 
# Usage
 - Launch the application on your iOS device or simulator.
 - Navigate to the shopping cart section.
 - Add items to the cart from the e-commerce display.
 - Adjust quantities using the SkyFloatingTextField component.
 - Access network functionality and camera features as required.
 - Experience the customized UI components and theme colors.
 - View and manage cart items with the provided functionalities.
 - Explore the implemented custom classes and extensions for further customization.
